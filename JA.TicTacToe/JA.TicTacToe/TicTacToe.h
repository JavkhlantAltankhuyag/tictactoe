#pragma once

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

class TicTacToe {


private:

	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

public:


	TicTacToe() : m_board{ '1', '2', '3', '4', '5', '6', '7', '8', '9' },
		m_numTurns(0), m_playerTurn('X'), m_winner(' ') { }


	char GetPlayerTurn() { return m_playerTurn; }

	void DisplayBoard() {

		cout << "\t     |     |     "<<"\n";
		cout << "\t  " << m_board[0] << "  |  " << m_board[1] << "  |  " << m_board[2] << "\n";
		cout << "\t_____|_____|_____" << "\n";
		cout << "\t     |     |     " << "\n";
		cout << "\t  " << m_board[3] << "  |  " << m_board[4] << "  |  " << m_board[5] << "\n";
		cout << "\t_____|_____|_____" << "\n";
		cout << "\t     |     |     " << "\n";
		cout << "\t  " << m_board[6] << "  |  " << m_board[7] << "  |  " << m_board[8] << "\n";
		cout << "\t     |     |     " << "\n";
	}

	bool IsOver() {

		if ((m_board[0] == m_board[4] && m_board[4] == m_board[8]) || (m_board[2] == m_board[4] && m_board[4] == m_board[6])) {
			m_winner = m_board[4];
			return true;
		}
		else if (m_board[0] == m_board[3] && m_board[3] == m_board[6]) {
			m_winner = m_board[0];
			return true;
		}
		else if (m_board[1] == m_board[4] && m_board[4] == m_board[7]) {
			m_winner = m_board[1];
			return true;
		}
		else if (m_board[2] == m_board[5] && m_board[5] == m_board[8]) {
			m_winner = m_board[2];
			return true;
		}
		else if (m_board[0] == m_board[1] && m_board[1] == m_board[2]) {
			m_winner = m_board[0];
			return true;
		}
		else if (m_board[3] == m_board[4] && m_board[4] == m_board[5]) {
			m_winner = m_board[3];
			return true;
		}
		else if (m_board[6] == m_board[7] && m_board[7] == m_board[8]) {
			m_winner = m_board[6];
			return true;
		}
		else if (m_numTurns == 9) {
			m_winner = 'T';
			return true;
		}
		else
			return false;

	}


	bool IsValidMove(int position) {

		if (m_board[position - 1] == 'X' || m_board[position - 1] == 'O')
			return false;
		else
			return true;
	}

	void Move(int position) {

		if (m_playerTurn == 'X') {
			m_board[position - 1] = 'X';
			m_playerTurn = 'O';
		}
		else {
			m_board[position - 1] = 'O';
			m_playerTurn = 'X';
		}

		m_numTurns++;
	}

	void DisplayResult() {

		if (m_winner == 'X')
			cout << "Player X won!" << "\n";
		if (m_winner == 'O')
			cout << "Player O won!" << "\n";
		if (m_winner == 'T')
			cout << "Tied!" << "\n";

	}

};